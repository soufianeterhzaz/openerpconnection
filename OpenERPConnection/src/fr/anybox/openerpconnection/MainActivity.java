package fr.anybox.openerpconnection;


import android.os.Bundle;
import android.os.AsyncTask.Status;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	OpenERPConnection oc = new OpenERPConnection();
	 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// get the button and set it's listener
		Button connect = (Button)findViewById(R.id.do_connection);
		OnClickListener l = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(oc.getStatus()==Status.FINISHED){
					//if AsyncTask is done, we have to create a new instance
					oc = new OpenERPConnection();
				}
				if(oc.getStatus()== Status.PENDING){
					// AsyncTask is ready to run
					oc.execute(getApplicationContext());
				}else if(oc.getStatus()== Status.PENDING) {
					//if AsynTask is already running, just wait and click again
					Toast.makeText(getApplicationContext(), R.string.it_s_already_running_try_again_later,Toast.LENGTH_LONG ).show();
				}
			}
		};
		connect.setOnClickListener(l );
	}


}
