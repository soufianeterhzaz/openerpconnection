package fr.anybox.openerpconnection;

import org.alexd.jsonrpc.JSONRPCException;
import org.alexd.jsonrpc.JSONRPCHttpClient;
import org.apache.http.client.HttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class OpenERPConnection extends AsyncTask<Context, Void, Void>{
	static final String TAG = "OERP";
	static final String HOST = "https://test.openerp.verkest.fr";
	static final String REQUEST_AUTHENTICATE = "/web/session/authenticate";
	static final String REQUEST_SEARCH_READ = "/web/dataset/search_read";
	static final String DATABASE = "oerp";
	static final String USER = "demo";
	static final String PASSWORD = "demo";
	static final String method = "call";
	
	@Override
	protected Void doInBackground(Context... params) {
		//The http client that allow to keep the session open over requests
		//HttpClient httpClient = new DefaultHttpClient();
		
		//The https client that allow to keep the session open over requests
		HttpClient httpClient = new AnyboxHttpsClient(params[0]);
		
		JSONRPCHttpClient jsonRPC_client = new JSONRPCHttpClient(httpClient, HOST + REQUEST_AUTHENTICATE);
		jsonRPC_client.setConnectionTimeout(2000);
		jsonRPC_client.setSoTimeout(2000);
		jsonRPC_client.setEncoding("UTF-8");
		
		try {
			//set params to send to OpenERP to create the connection
			JSONObject jsonParams = new JSONObject();
			jsonParams.put("db", DATABASE);
			jsonParams.put("login", USER);
			jsonParams.put("password",USER);
			
			//Do the connection with openERP
			JSONObject json_result = jsonRPC_client.callJSONObject(method, jsonParams);
			Log.d(TAG, "We are connect to OpenERP, session id: " + json_result.getString("session_id"));
	
			//Now we can request partner using the same session (http client)
			jsonRPC_client = new JSONRPCHttpClient(httpClient, HOST + REQUEST_SEARCH_READ);
			jsonRPC_client.setConnectionTimeout(2000);
			jsonRPC_client.setSoTimeout(2000);
			jsonRPC_client.setEncoding("UTF-8");

			//get the user_context from the connection result, to send to OpenERP in the next request
			JSONObject context = json_result.getJSONObject("user_context");
			
			//set params to send to OpenERP service
			jsonParams = new JSONObject();
			jsonParams.put("session_id", json_result.getString("session_id"));
			jsonParams.put("context", context);
			jsonParams.put("model", "res.partner");
			jsonParams.put("limit",10);
			
			//Domain is use in openerp to define the where sql
			JSONArray domain =  new JSONArray();
			JSONArray customer = new JSONArray();
			JSONArray person = new JSONArray();
			//Get only customers
			customer.put("customer");
			customer.put("=");
			customer.put("1");
			domain.put(customer); 
			//Get only person (no companies)
			person.put("is_company");
			person.put("=");
			person.put("0");
			domain.put(customer); 
			jsonParams.put("domain",domain); //domain : [[customer, =, 1],[is_company, =, 0]]
											// WHERE customer = 1 AND is_company = 0
			//choose fields you want to get
			JSONArray fields = new JSONArray();
			fields.put("name");
			fields.put("city");
			
			jsonParams.put("fields", fields);
			//jsonParams.put("offset",0);
			jsonParams.put("sort","write_date desc");
			
			//send the request to openerp
			json_result = jsonRPC_client.callJSONObject(method, jsonParams);
			JSONArray customers = json_result.getJSONArray("records");
			JSONObject oerp_customer;
			for(int i =0 ; i<10;i++){
				oerp_customer = customers.getJSONObject(i);
				Log.d(TAG, oerp_customer.getString("name") + " lives in " + oerp_customer.getString("city"));
			}	
			

		} catch (JSONException e) {
			Log.e(TAG,"Json exception: " + e.getMessage(), e);
			
		} catch (JSONRPCException e) {
			Log.e(TAG,"Json-rpc exception: " + e.getMessage(), e);
		}
		
		return null;
	}

}
